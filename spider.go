package main

import (
	"errors"
	"fmt"
	"gitlab.com/g3n/engine/core"
	"gitlab.com/g3n/engine/math32"
	"gitlab.com/gamerscomplete/kinematics"
	"math"
)

const (
	coxaMin  = -120
	coxaMax  = 120
	femurMin = -180
	femurMax = 180
	tibiaMin = -120
	tibiaMax = 120

	coxaLen  = 83
	femurLen = 157
	tibiaLen = 233

	bodyWidth  = 230
	bodyLength = 256
	bodyHeight = 40
)

type SpiderModel struct {
	legs    []Leg
	changed bool
}

type Leg struct {
	Joints     [3]*Joint
	CoxaOffset math32.Vector3
}

type JointType int

const (
	Coxa JointType = iota
	Femur
	Tibia
)

type Joint struct {
	*core.Node
	axis    math32.Vector3
	offset  float32
	angle   float32
	inverse bool
	changed bool
}

func (spider *SpiderModel) SetChanged(changed bool) {
	spider.changed = changed
}

func NewSpiderModel() *SpiderModel {
	return &SpiderModel{
		legs: make([]Leg, 6),
	}
}

func (spider *SpiderModel) Changed() bool {
	return spider.changed
}

func (joint *Joint) GetAngle() float32 {
	return joint.angle
}

func (joint *Joint) Changed() bool {
	return joint.changed
}

func (joint *Joint) SetChanged(changed bool) {
	joint.changed = changed
}

// Set the joints angle in degrees
func (joint *Joint) SetAngle(angle float32) {
	if joint == nil {
		fmt.Println("joint nil")
		return
	}
	var quat math32.Quaternion
	if joint.inverse {
		quat.SetFromAxisAngle(&joint.axis, math32.DegToRad(-angle+joint.offset))
	} else {
		quat.SetFromAxisAngle(&joint.axis, math32.DegToRad(angle+joint.offset))
	}
	joint.SetQuaternionQuat(&quat)
	joint.angle = angle
	joint.SetChanged(true)
}

func (spider *SpiderModel) SetEffectorPosition(leg int, x, y, z float32) error {
	coxa, femur, tibia, err := spider.legs[leg].ComputeIK(math32.Vector3{x, y, z})
	if err != nil {
		return errors.New("Failed to set joint angle: " + err.Error())
	}
	spider.legs[leg].Joints[Coxa].SetAngle(float32(radToDeg(coxa)))
	fmt.Println("Setting coxa to:", coxa, "in degree", radToDeg(coxa))
	spider.legs[leg].Joints[Femur].SetAngle(float32(radToDeg(femur)))
	fmt.Println("Setting femur to:", femur, "in degree", radToDeg(femur))
	spider.legs[leg].Joints[Tibia].SetAngle(float32(radToDeg(tibia)))
	fmt.Println("Setting tibia to:", tibia, "in degree", radToDeg(tibia))

	return nil
}

// InverseKinematics calculates the joint angles required to move the end effector (foot)
// of a single leg to the specified position and orientation.
/*func InverseKinematics(x, y, z float64) (coxa, femur, tibia float64, ok bool) {
	// Solve for the coxa angle
	coxa = radToDeg(math.Atan2(y, x))
	if coxa < coxaMin || coxa > coxaMax {
		return 0, 0, 0, false
	}

	// Calculate the distance from the base of the leg to the end effector
	d := math.Sqrt(x*x + y*y)

	// Calculate the position of the end effector projected onto the x-y plane
	x0 := d - coxaLength
	z0 := z

	// Calculate the length of the projection of the leg onto the x-y plane
	l := math.Sqrt(x0*x0 + z0*z0)

	// Solve for the femur and tibia angles
	cosTibia := (femurLength*femurLength + tibiaLength*tibiaLength - l*l) / (2 * femurLength * tibiaLength)
	tibiaRad := math.Acos(cosTibia)
	femurRad := math.Atan2(z0, x0) - math.Atan2(tibiaLength*math.Sin(tibiaRad), femurLength+tibiaLength*cosTibia)
	femur = radToDeg(femurRad) - femurAngle
	tibia = radToDeg(tibiaRad) - tibiaAngle

	// Check if the solution is valid
	if femur < femurMin || femur > femurMax || tibia < tibiaMin || tibia > tibiaMax {
		fmt.Println("Femur:", femur, " Tibia: ", tibia)
		return 0, 0, 0, false
	}

	return coxa - coxaAngle, femur, tibia, true
}
*/

func (leg *Leg) ComputeIK(target math32.Vector3) (coxaAngle, femurAngle, tibiaAngle float64, err error) {
	coxaAngle, femurAngle, tibiaAngle, err = kinematics.InverseKinematics(float64(target.X), float64(target.Y), float64(target.Z), coxaLen, femurLen, tibiaLen)
	/*
		// Compute the distance between the base of the leg and the target point
		x := float64(target.X - leg.CoxaOffset.X)
		y := float64(target.Y - leg.CoxaOffset.Y)
		z := float64(target.Z - leg.CoxaOffset.Z)
		distance := math.Sqrt(x*x + y*y + z*z)

		if distance > coxaLength+femurLength+tibiaLength || distance < coxaLength-femurLength-tibiaLength {
			// Target is out of reach, return an error
			err = errors.New("target point is out of reach")
			return
		}

		// Compute the coxa angle
		coxaAngle = math.Atan2(y, x)

		// Compute the distance between the end of the coxa and the target point projected onto the x-y plane
		projDistance := math.Sqrt(x*x+y*y) - coxaLength

		// Compute the distance between the end of the coxa and the target point in the z-axis
		actualZ := z - tibiaLength
		hypotenuse := math.Sqrt(projDistance*projDistance + actualZ*actualZ)

		// Compute the femur angle
		beta := math.Acos((femurLength*femurLength + hypotenuse*hypotenuse - tibiaLength*tibiaLength) / (2 * femurLength * hypotenuse))
		femurAngle = math.Atan2(actualZ, projDistance) + math.Atan2(hypotenuse*math.Sin(beta), femurLength+hypotenuse*math.Cos(beta))

		if math.IsNaN(femurAngle) {
			// The input parameters are invalid, return an error
			err = errors.New("invalid input parameters")
			return
		}

		// Compute the tibia angle
		gamma := math.Acos((femurLength*femurLength + tibiaLength*tibiaLength - hypotenuse*hypotenuse) / (2 * femurLength * tibiaLength))
		tibiaAngle = math.Pi - gamma
	*/
	return
}

// degToRad converts an angle in degrees to radians.
func degToRad(deg float64) float64 {
	return deg * (math.Pi / 180)
}

// radToDeg converts an angle in radians to degrees.
func radToDeg(rad float64) float64 {
	return rad * (180 / math.Pi)
}

func (leg *Leg) ComputeFK() math32.Vector3 {
	endY, endX, endZ := kinematics.ForwardKinematics(float64(leg.Joints[Coxa].angle), float64(leg.Joints[Femur].angle), float64(leg.Joints[Tibia].angle), coxaLen, femurLen, tibiaLen)
	/*


		// Compute the position of the end of the coxa
		coxaX := coxaLength * math.Cos(degToRad(float64(leg.Joints[Coxa].angle)))
		coxaY := coxaLength * math.Sin(degToRad(float64(leg.Joints[Coxa].angle)))

		// Compute the position of the end of the femur relative to the end of the coxa
		femurX := (femurLength + tibiaLength*math.Cos(degToRad(float64(leg.Joints[Tibia].angle)))) * math.Cos(degToRad(float64(leg.Joints[Femur].angle)))
		femurY := (femurLength + tibiaLength*math.Cos(degToRad(float64(leg.Joints[Tibia].angle)))) * math.Sin(degToRad(float64(leg.Joints[Femur].angle)))
		femurZ := float64(tibiaLength) * math.Sin(degToRad(float64(leg.Joints[Tibia].angle)))

		// Compute the total position of the end of the leg
		endX := coxaX + femurX
		endY := coxaY + femurY
		endZ := femurZ - float64(leg.CoxaOffset.Z)
	*/

	return math32.Vector3{
		X: float32(endX + float64(leg.CoxaOffset.X)),
		Y: float32(endY + float64(leg.CoxaOffset.Y)),
		Z: float32(endZ + float64(leg.CoxaOffset.Z)),
	}
}
