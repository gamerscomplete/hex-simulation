package main

import (
	"gitlab.com/g3n/engine/core"
	"gitlab.com/g3n/engine/gui"
	"gitlab.com/gamerscomplete/hex-simulation/app"
)

type HexSim struct {
	rootScene    *core.Node
	runScene     *RunScene
	currentScene Scene

	app *app.Application
}

func NewHexSim() *HexSim {
	return &HexSim{}
}

func (sim *HexSim) switchScene(scene Scene) {
	sim.rootScene.Remove(sim.currentScene)
	sim.rootScene.Add(scene)
	if sim.currentScene != nil {
		sim.currentScene.Stop()
	}
	sim.currentScene = scene
	gui.Manager().Set(sim.currentScene)
	scene.Start()
}

// TODO: Populate this with the loop, fps should be calculated here
func (sim *HexSim) Update() {

}
