package main

import (
	"fmt"
	"gitlab.com/g3n/engine/camera"
	"gitlab.com/g3n/engine/core"
	"gitlab.com/g3n/engine/gls"
	"gitlab.com/g3n/engine/gui"
	"gitlab.com/g3n/engine/math32"
	"gitlab.com/g3n/engine/renderer"
	"gitlab.com/g3n/engine/window"
	"gitlab.com/gamerscomplete/hex-simulation/app"
	"time"
)

func main() {

	// Create application and scene
	a := app.App("Hex Sim", 1280, 764)
	//	a.SetFullScreen(true)

	sim := NewHexSim()
	sim.app = a
	sim.rootScene = core.NewNode()

	// Set the scene to be managed by the gui manager
	gui.Manager().Set(sim.rootScene)

	// Create perspective camera
	width, height := sim.app.GetSize()
	aspect := float32(width) / float32(height)
	cam := camera.New(aspect)
	cam.UpdateSize(5)
	//x, y, z
	//x, y, height
	cam.SetPosition(0, -400, 1000)
	cam.LookAt(&math32.Vector3{0, 0, 0}, &math32.Vector3{0, 1, 0})
	cam.SetProjection(camera.Perspective)

	cam.SetFar(10240)

	sim.rootScene.Add(cam)

	// Set up orbit control for the camera
	camera.NewOrbitControl(cam)

	// Set up callback to update viewport and camera aspect ratio when the window is resized
	onResize := func(evname string, ev interface{}) {
		// Get framebuffer size and update viewport accordingly
		width, height := a.GetSize()
		a.Gls().Viewport(0, 0, int32(width), int32(height))
		// Update the camera's aspect ratio
		cam.SetAspect(float32(width) / float32(height))
	}
	a.Subscribe(window.OnWindowSize, onResize)
	onResize("", nil)

	// Create and add a button to the scene

	runScene, err := NewRunScene(sim.app)
	if err != nil {
		fmt.Println("Failed to setup run scene:", err)
		return
	}
	sim.runScene = runScene

	sim.switchScene(sim.runScene)

	// Set background color to gray
	a.Gls().ClearColor(0.5, 0.5, 0.5, 1.0)

	// Run the application
	a.Run(func(renderer *renderer.Renderer, deltaTime time.Duration) {
		a.Gls().Clear(gls.DEPTH_BUFFER_BIT | gls.STENCIL_BUFFER_BIT | gls.COLOR_BUFFER_BIT)
		renderer.Render(sim.rootScene, cam)
	})
}
