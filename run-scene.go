package main

import (
	"fmt"
	"gitlab.com/g3n/engine/core"
	"gitlab.com/g3n/engine/geometry"
	"gitlab.com/g3n/engine/graphic"
	"gitlab.com/g3n/engine/gui"
	"gitlab.com/g3n/engine/light"
	"gitlab.com/g3n/engine/material"
	"gitlab.com/g3n/engine/math32"
	"gitlab.com/gamerscomplete/hex-simulation/app"
	"strconv"
	"time"
)

type RunScene struct {
	*core.Node
	monitor monitor
	warnFPS int

	spiderModel *SpiderModel
}

type monitor struct {
	stopMonitor chan bool
	fpsLabel    *gui.Label
	fpsText     string
	statsTable  *gui.Table
}

func NewRunScene(application *app.Application) (*RunScene, error) {
	runScene := &RunScene{
		Node: core.NewNode(),
	}

	// Label
	fpsWidth := 200
	fpsHeight := 50
	runScene.warnFPS = 20
	width, _ := application.GetSize()
	runScene.monitor.fpsText = "FPS: "
	runScene.monitor.fpsLabel = gui.NewLabel(runScene.monitor.fpsText)
	//-10 to give it the same buffer from the screen edge as the top
	runScene.monitor.fpsLabel.SetPosition(float32(width-fpsWidth-10), 10)
	runScene.monitor.fpsLabel.SetPaddings(2, 2, 2, 2)
	runScene.monitor.fpsLabel.SetFontSize(20)
	runScene.monitor.fpsLabel.SetSize(float32(fpsWidth), float32(fpsHeight))
	runScene.Add(runScene.monitor.fpsLabel)

	runScene.createSpider()

	//////////TESTING
	fmt.Println("leg 1 pre set:", runScene.spiderModel.legs[0].ComputeFK())
	//x is forwards/backwards and positive is backwards
	//y is left to right, positive is towards the body
	//z seems to be height
	//Setting leg 1 to an initial position
	if err := runScene.spiderModel.SetEffectorPosition(0, 0, 350, 0); err != nil {
		fmt.Println("Failed to set effector:", err)
	} else {
		fmt.Println("Setting effector successful")
	}
	fmt.Println("leg 1 post set:", runScene.spiderModel.legs[0].ComputeFK())
	/////////////////

	// Adds directional light
	l1 := light.NewDirectional(&math32.Color{0.4, 0.4, 0.4}, 1.0)
	l1.SetPosition(0, 0, 10)
	runScene.Add(l1)

	ambLight := light.NewAmbient(&math32.Color{1.0, 1.0, 1.0}, 0.5)
	runScene.Add(ambLight)

	formatAngle := func(cell gui.TableCell) string {
		if cell.Value == nil {
			return ""
		}
		angle := cell.Value.(float32)
		return strconv.FormatFloat(float64(angle), 'f', 2, 32)
	}
	//Add data output table
	tab, err := gui.NewTable(150, 500, []gui.TableColumn{
		{Id: "1", Header: "ID", Width: 20, Minwidth: 32, Align: gui.AlignCenter, Sort: gui.TableSortNumber, Expand: 0, Hidden: true},
		{Id: "2", Header: "Leg/Joint", Width: 20, Minwidth: 32, Align: gui.AlignCenter, Resize: true, Expand: 0},
		{Id: "3", Header: "Angle", Width: 95, Minwidth: 32, Align: gui.AlignCenter, FormatFunc: formatAngle, Resize: true, Expand: 0},
	})
	if err != nil {
		return nil, err
	}

	tab.SetBorders(1, 1, 1, 1)
	tab.SetPosition(0, 0)
	tab.SetMargins(10, 10, 10, 10)
	tab.ShowStatus(true)
	//	tab.SetHeight(600)

	rows := make([]map[string]interface{}, 0, 18)
	for i := 0; i < 6; i++ {
		for i2 := 1; i2 <= 3; i2++ {
			row := make(map[string]interface{})
			row["1"] = i*3 + i2
			row["2"] = strconv.Itoa(i+1) + "/" + strconv.Itoa(i2)
			row["3"] = float32(0.0)
			rows = append(rows, row)
		}
	}
	tab.SetRows(rows)
	runScene.Add(tab)

	//	var selectedLeg int

	// DropDown simple
	dd1 := gui.NewDropDown(150, gui.NewImageLabel("init"))
	dd1.SetPosition(170, 10)
	runScene.Add(dd1)
	for i := 1; i <= 6; i++ {
		dd1.Add(gui.NewImageLabel(fmt.Sprintf("Leg %2d", i)))
	}
	dd1.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		fmt.Println(dd1.SelectedPos())
	})
	dd1.SetSelected(dd1.ItemAt(0))

	// Slider 1
	s1 := gui.NewVSlider(64, 400)
	s1.SetPosition(170, dd1.Position().Y+dd1.Height()+10)
	s1.SetValue(0.5)
	s1.SetText(fmt.Sprintf("X: %1.2f", s1.Value()))
	s1.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		s1.SetText(fmt.Sprintf("X: %1.2f", s1.Value()))
	})
	runScene.Add(s1)

	// Slider 2
	s2 := gui.NewVSlider(64, 400)
	s2.SetPosition(s1.Position().X+s1.Width()+20, dd1.Position().Y+dd1.Height()+10)
	s2.SetValue(0.5)
	s2.SetText(fmt.Sprintf("Y: %1.2f", s2.Value()))
	s2.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		s2.SetText(fmt.Sprintf("Y: %1.2f", s2.Value()))
	})
	runScene.Add(s2)

	// Slider 3
	s3 := gui.NewVSlider(64, 400)
	s3.SetPosition(s2.Position().X+s2.Width()+20, dd1.Position().Y+dd1.Height()+10)
	s3.SetValue(0.5)
	s3.SetText(fmt.Sprintf("Z: %1.2f", s3.Value()))
	s3.Subscribe(gui.OnChange, func(evname string, ev interface{}) {
		s3.SetText(fmt.Sprintf("Z: %1.2f", s3.Value()))
	})
	runScene.Add(s3)

	runScene.monitor.statsTable = tab

	runScene.monitor.stopMonitor = make(chan bool)

	return runScene, nil
}

func (runScene *RunScene) Start() {
	go runScene.startMonitor()
}
func (runScene *RunScene) Stop() {
	runScene.monitor.stopMonitor <- true
}

func (runScene *RunScene) startMonitor() {
	ticker := time.NewTicker(1 * time.Second)

	for {
		select {
		case <-ticker.C:
			fps := 0
			//TODO: Caclculate fps, wont matter too much right now since were on a 1 second ticker
			runScene.monitor.fpsLabel.SetText(runScene.monitor.fpsText + strconv.Itoa(fps))
			if fps < runScene.warnFPS {
				runScene.monitor.fpsLabel.SetBgColor(math32.NewColor("red"))
			} else {
				runScene.monitor.fpsLabel.SetBgColor(math32.NewColor("green"))
			}
			//update the table on the side displaying stats
			for k, _ := range runScene.spiderModel.legs {
				for k2, v := range runScene.spiderModel.legs[k].Joints {
					if v.Changed() {
						runScene.monitor.statsTable.SetCell(k*3+k2, "3", v.GetAngle())
						v.SetChanged(false)
					}
				}
			}
		case <-runScene.monitor.stopMonitor:
			ticker.Stop()
			break
		}
	}
}

func (runScene *RunScene) createSpider() {
	spiderModel := NewSpiderModel()

	bodyLength := float32(256.0)
	bodyThickness := float32(40)
	bodyWidth := float32(230.0)

	appendageThickness := float32(40)
	appendageWidth := float32(40)

	coxaLength := float32(83.0)
	femurLength := float32(157.0)
	tibiaLength := float32(233.0)

	//Create body of spider
	spiderRootNode := core.NewNode()
	bodyMat := material.NewStandard(&math32.Color{0.5, 0, 0})
	coxaMat := material.NewStandard(&math32.Color{0, 0.5, 0})
	femurMat := material.NewStandard(&math32.Color{0, 0, 0.5})
	tibiaMat := material.NewStandard(&math32.Color{0.5, 0, 0.5})

	bodyGeom := geometry.NewSegmentedBox(bodyWidth, bodyLength, bodyThickness, 1, 1, 1)
	body := graphic.NewMesh(bodyGeom, bodyMat)
	body.SetPosition(0, 0, 0)
	spiderRootNode.Add(body)

	coxaGeom := geometry.NewSegmentedBox(appendageWidth, coxaLength, appendageThickness, 1, 1, 1)
	femurGeom := geometry.NewSegmentedBox(appendageWidth, femurLength, appendageThickness, 1, 1, 1)
	tibiaGeom := geometry.NewSegmentedBox(appendageWidth, tibiaLength, appendageThickness, 1, 1, 1)

	// Leg 1
	coxaJoint1 := core.NewNode()
	coxa1 := graphic.NewMesh(coxaGeom, coxaMat)
	//move the joint half its own distance away from the body
	coxa1.TranslateX(coxaLength * .5)
	//rotate the joint to point away from the body
	coxa1.RotateZ(math32.DegToRad(90))
	//add the mesh to the joint node
	coxaJoint1.Add(coxa1)
	//Move joint to the side of body
	coxaJoint1.TranslateX(bodyWidth * .5)
	coxaJoint1.TranslateY(bodyLength * .5)
	body.Add(coxaJoint1)

	femurJoint1 := core.NewNode()
	femur1 := graphic.NewMesh(femurGeom, femurMat)
	femurJoint1.Add(femur1)

	coxaJoint1.Add(femurJoint1)
	femur1.TranslateX(femurLength * 0.5)
	femur1.RotateZ(math32.DegToRad(90))
	femurJoint1.TranslateX(coxaLength)

	tibiaJoint1 := core.NewNode()
	tibia1 := graphic.NewMesh(tibiaGeom, tibiaMat)
	tibiaJoint1.Add(tibia1)
	femurJoint1.Add(tibiaJoint1)
	tibia1.TranslateX(tibiaLength * 0.5)
	tibia1.RotateZ(math32.DegToRad(90))
	tibiaJoint1.TranslateX(femurLength)

	// Leg 2
	coxaJoint2 := core.NewNode()
	coxa2 := graphic.NewMesh(coxaGeom, coxaMat)
	//move the joint half its own distance away from the body
	coxa2.TranslateX(coxaLength * .5)
	//rotate the joint to point away from the body
	coxa2.RotateZ(math32.DegToRad(90))
	//add the mesh to the joint node
	coxaJoint2.Add(coxa2)
	//Move joint to the side of body
	coxaJoint2.TranslateX(bodyWidth * .5)
	body.Add(coxaJoint2)

	femurJoint2 := core.NewNode()
	femur2 := graphic.NewMesh(femurGeom, femurMat)
	femurJoint2.Add(femur2)

	coxaJoint2.Add(femurJoint2)
	femur2.TranslateX(femurLength * 0.5)
	femur2.RotateZ(math32.DegToRad(90))
	femurJoint2.TranslateX(coxaLength)

	tibiaJoint2 := core.NewNode()
	tibia2 := graphic.NewMesh(tibiaGeom, tibiaMat)
	tibiaJoint2.Add(tibia2)
	femurJoint2.Add(tibiaJoint2)
	tibia2.TranslateX(tibiaLength * 0.5)
	tibia2.RotateZ(math32.DegToRad(90))
	tibiaJoint2.TranslateX(femurLength)

	// Leg 3
	coxaJoint3 := core.NewNode()
	coxa3 := graphic.NewMesh(coxaGeom, coxaMat)
	//move the joint half its own distance away from the body
	coxa3.TranslateX(coxaLength * .5)
	//rotate the joint to point away from the body
	coxa3.RotateZ(math32.DegToRad(90))
	//add the mesh to the joint node
	coxaJoint3.Add(coxa3)
	//Move joint to the side of body
	coxaJoint3.TranslateX(bodyWidth * .5)
	coxaJoint3.TranslateY(-bodyLength * .5)
	body.Add(coxaJoint3)

	femurJoint3 := core.NewNode()
	femur3 := graphic.NewMesh(femurGeom, femurMat)
	femurJoint3.Add(femur3)

	coxaJoint3.Add(femurJoint3)
	femur3.TranslateX(femurLength * 0.5)
	femur3.RotateZ(math32.DegToRad(90))
	femurJoint3.TranslateX(coxaLength)

	tibiaJoint3 := core.NewNode()
	tibia3 := graphic.NewMesh(tibiaGeom, tibiaMat)
	tibiaJoint3.Add(tibia3)
	femurJoint3.Add(tibiaJoint3)
	tibia3.TranslateX(tibiaLength * 0.5)
	tibia3.RotateZ(math32.DegToRad(90))
	tibiaJoint3.TranslateX(femurLength)

	// Leg 4
	coxaJoint4 := core.NewNode()
	coxa4 := graphic.NewMesh(coxaGeom, coxaMat)
	//move the joint half its own distance away from the body
	coxa4.TranslateX(coxaLength * .5)
	//rotate the joint to point away from the body
	coxa4.RotateZ(math32.DegToRad(90))
	//add the mesh to the joint node
	coxaJoint4.Add(coxa4)
	//Move joint to the side of body
	coxaJoint4.TranslateX(-bodyWidth * .5)
	coxaJoint4.TranslateY(-bodyLength * .5)
	body.Add(coxaJoint4)

	femurJoint4 := core.NewNode()
	femur4 := graphic.NewMesh(femurGeom, femurMat)
	femurJoint4.Add(femur4)

	coxaJoint4.Add(femurJoint4)
	femur4.TranslateX(femurLength * 0.5)
	femur4.RotateZ(math32.DegToRad(90))
	femurJoint4.TranslateX(coxaLength)

	tibiaJoint4 := core.NewNode()
	tibia4 := graphic.NewMesh(tibiaGeom, tibiaMat)
	tibiaJoint4.Add(tibia4)
	femurJoint4.Add(tibiaJoint4)
	tibia4.TranslateX(tibiaLength * 0.5)
	tibia4.RotateZ(math32.DegToRad(90))
	tibiaJoint4.TranslateX(femurLength)
	coxaJoint4.RotateY(math32.DegToRad(180))
	coxaJoint4.RotateX(math32.DegToRad(180))

	// Leg 5
	coxaJoint5 := core.NewNode()
	coxa5 := graphic.NewMesh(coxaGeom, coxaMat)
	//move the joint half its own distance away from the body
	coxa5.TranslateX(coxaLength * .5)
	//rotate the joint to point away from the body
	coxa5.RotateZ(math32.DegToRad(90))
	//add the mesh to the joint node
	coxaJoint5.Add(coxa5)
	//Move joint to the side of body
	coxaJoint5.TranslateX(-bodyWidth * .5)
	body.Add(coxaJoint5)

	femurJoint5 := core.NewNode()
	femur5 := graphic.NewMesh(femurGeom, femurMat)
	femurJoint5.Add(femur5)

	coxaJoint5.Add(femurJoint5)
	femur5.TranslateX(femurLength * 0.5)
	femur5.RotateZ(math32.DegToRad(90))
	femurJoint5.TranslateX(coxaLength)

	tibiaJoint5 := core.NewNode()
	tibia5 := graphic.NewMesh(tibiaGeom, tibiaMat)
	tibiaJoint5.Add(tibia5)
	femurJoint5.Add(tibiaJoint5)
	tibia5.TranslateX(tibiaLength * 0.5)
	tibia5.RotateZ(math32.DegToRad(90))
	tibiaJoint5.TranslateX(femurLength)
	coxaJoint5.RotateY(math32.DegToRad(180))
	coxaJoint5.RotateX(math32.DegToRad(180))

	// Leg 6
	coxaJoint6 := core.NewNode()
	coxa6 := graphic.NewMesh(coxaGeom, coxaMat)
	//move the joint half its own distance away from the body
	coxa6.TranslateX(coxaLength * .5)
	//rotate the joint to point away from the body
	coxa6.RotateZ(math32.DegToRad(90))
	//add the mesh to the joint node
	coxaJoint6.Add(coxa6)
	//Move joint to the side of body
	coxaJoint6.TranslateX(-bodyWidth * .5)
	coxaJoint6.TranslateY(bodyLength * .5)
	body.Add(coxaJoint6)

	femurJoint6 := core.NewNode()
	femur6 := graphic.NewMesh(femurGeom, femurMat)
	femurJoint6.Add(femur6)

	coxaJoint6.Add(femurJoint6)
	femur6.TranslateX(femurLength * 0.5)
	femur6.RotateZ(math32.DegToRad(90))
	femurJoint6.TranslateX(coxaLength)

	tibiaJoint6 := core.NewNode()
	tibia6 := graphic.NewMesh(tibiaGeom, tibiaMat)
	tibiaJoint6.Add(tibia6)
	femurJoint6.Add(tibiaJoint6)
	tibia6.TranslateX(tibiaLength * 0.5)
	tibia6.RotateZ(math32.DegToRad(90))
	tibiaJoint6.TranslateX(femurLength)
	coxaJoint6.RotateY(math32.DegToRad(180))
	coxaJoint6.RotateX(math32.DegToRad(180))

	spiderModel.legs[0].Joints[Coxa] = &Joint{Node: coxaJoint1, axis: math32.Vector3{0, 0, 1}, offset: 90, inverse: true}
	spiderModel.legs[0].Joints[Femur] = &Joint{Node: femurJoint1, axis: math32.Vector3{0, 1, 0}, offset: -90}
	spiderModel.legs[0].Joints[Tibia] = &Joint{Node: tibiaJoint1, axis: math32.Vector3{0, 1, 0}, offset: -90}

	spiderModel.legs[1].Joints[Coxa] = &Joint{Node: coxaJoint2, axis: math32.Vector3{0, 0, 1}, offset: 90, inverse: true}
	spiderModel.legs[1].Joints[Femur] = &Joint{Node: femurJoint2, axis: math32.Vector3{0, 1, 0}, offset: -90}
	spiderModel.legs[1].Joints[Tibia] = &Joint{Node: tibiaJoint2, axis: math32.Vector3{0, 1, 0}, offset: -90}

	spiderModel.legs[2].Joints[Coxa] = &Joint{Node: coxaJoint3, axis: math32.Vector3{0, 0, 1}, offset: 90, inverse: true}
	spiderModel.legs[2].Joints[Femur] = &Joint{Node: femurJoint3, axis: math32.Vector3{0, 1, 0}, offset: -90}
	spiderModel.legs[2].Joints[Tibia] = &Joint{Node: tibiaJoint3, axis: math32.Vector3{0, 1, 0}, offset: -90}

	spiderModel.legs[3].Joints[Coxa] = &Joint{Node: coxaJoint4, axis: math32.Vector3{0, 0, 1}, offset: -90, inverse: true}
	spiderModel.legs[3].Joints[Femur] = &Joint{Node: femurJoint4, axis: math32.Vector3{0, 1, 0}, offset: -90}
	spiderModel.legs[3].Joints[Tibia] = &Joint{Node: tibiaJoint4, axis: math32.Vector3{0, 1, 0}, offset: -90}

	spiderModel.legs[4].Joints[Coxa] = &Joint{Node: coxaJoint5, axis: math32.Vector3{0, 0, 1}, offset: -90, inverse: true}
	spiderModel.legs[4].Joints[Femur] = &Joint{Node: femurJoint5, axis: math32.Vector3{0, 1, 0}, offset: -90}
	spiderModel.legs[4].Joints[Tibia] = &Joint{Node: tibiaJoint5, axis: math32.Vector3{0, 1, 0}, offset: -90}

	spiderModel.legs[5].Joints[Coxa] = &Joint{Node: coxaJoint6, axis: math32.Vector3{0, 0, 1}, offset: -90, inverse: true}
	spiderModel.legs[5].Joints[Femur] = &Joint{Node: femurJoint6, axis: math32.Vector3{0, 1, 0}, offset: -90}
	spiderModel.legs[5].Joints[Tibia] = &Joint{Node: tibiaJoint6, axis: math32.Vector3{0, 1, 0}, offset: -90}

	runScene.spiderModel = spiderModel
	/*
	   	fmt.Println("leg 1:", spiderModel.legs[0].ComputeFK())



	   	//x is forwards/backwards and positive is backwards
	   	//y is left to right, positive is towards the body
	   	//z seems to be height


	   	//Setting leg 1 to an initial position
	   	if err := spiderModel.SetEffectorPosition(0, 0, 150, 0); err != nil {
	   //	if err := spiderModel.SetEffectorPosition(0, 50, 100, 50); err != nil {
	   		fmt.Println("Failed to set effector:", err)
	   	} else {
	   		fmt.Println("Setting effector successful")
	   	}
	   	fmt.Println("leg 1:", spiderModel.legs[0].ComputeFK())
	*/
	runScene.Add(spiderRootNode)
}
