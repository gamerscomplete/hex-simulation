module gitlab.com/gamerscomplete/hex-simulation

go 1.23.2

require (
	gitlab.com/g3n/engine v0.2.0
	gitlab.com/gamerscomplete/kinematics v0.0.0-20241210075246-410c17e26c4b
)

require (
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20221017161538-93cebf72946b // indirect
	github.com/goki/freetype v0.0.0-20220119013949-7a161fd3728c // indirect
	golang.org/x/image v0.6.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
